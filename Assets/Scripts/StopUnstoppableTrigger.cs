﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StopUnstoppableTrigger : MonoBehaviour
{

    public PostProcessing postProcessing;
    public AgentMovement player;
    public AudioClip unstoppableSound;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            if (player.forceRightKeyPress && player.forceJumpPress)
            {
                player.forceRightKeyPress = false;
                player.forceJumpPress = false;
                postProcessing.SetNormalPostProcessing();
            }
        }
    }
}