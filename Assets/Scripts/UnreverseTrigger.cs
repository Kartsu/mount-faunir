﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public class UnreverseTrigger : MonoBehaviour
{
    public PostProcessing postProcessing;
    private AgentMovement player;
    public AudioClip reverseSound;


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            player = collision.GetComponent<AgentMovement>();
            if (player.isReverse)
            {
                player.isReverse = false;
                postProcessing.SetNormalPostProcessing();
                player.audioSource.PlayOneShot(reverseSound);
            }
        }
    }
}
