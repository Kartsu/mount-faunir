﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerArea : MonoBehaviour
{

    public GameObject objectToTurnOn;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.CompareTag("Player"))
        {
            if (!objectToTurnOn.activeSelf)
            {
                objectToTurnOn.SetActive(true);
            }
        }
    }
}
