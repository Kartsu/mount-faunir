﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndGameTrigger : MonoBehaviour
{
    public AgentMovement player;
    public GameObject endGameText;
    public GameObject endGameAnimationImage;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            //End the game
            player.canMove = false;
            endGameText.SetActive(true);
            endGameAnimationImage.SetActive(true);
        }
    }

    public void EndGame()
    {
        Application.Quit();
    }
}
