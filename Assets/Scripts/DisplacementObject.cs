﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisplacementObject : MonoBehaviour
{

    public Transform transformPositionBefore;
    public AgentMovement player;

    // Start is called before the first frame update
    void Start()
    {
        player.Respawned += ResetPos;
    }

    private void OnEnable()
    {
        transform.position = transformPositionBefore.position;
    }


    public void ResetPos()
    {
        transform.position = transformPositionBefore.position;
        gameObject.SetActive(false);
    }
}
