﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnstoppableTrigger : MonoBehaviour
{

    public PostProcessing postProcessing;
    private AgentMovement player;
    public AudioClip unstoppableSound;


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            player = collision.GetComponent<AgentMovement>();
            if(!player.forceRightKeyPress && !player.forceJumpPress) {
                postProcessing.SetUnstoppablePostProcessing();
                player.forceRightKeyPress = true;
                player.forceJumpPress = true;
                player.audioSource.PlayOneShot(unstoppableSound);
                player.Respawned += Stop;
            }
        }
    }

    private void Stop()
    {
        if (player.forceRightKeyPress && player.forceJumpPress)
        {
            player.forceRightKeyPress = false;
            player.forceJumpPress = false;
            postProcessing.SetNormalPostProcessing();
        }
        player.Respawned -= Stop;
    }
}