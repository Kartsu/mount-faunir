﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D;

public class AppearingPlatform : MonoBehaviour
{

    private SpriteShapeRenderer sr;
    //private SpriteShapeController src;
    // Start is called before the first frame update
    void Start()
    {
        //src = GetComponent<SpriteShapeController>();
        sr = GetComponent<SpriteShapeRenderer>();
        DisableSpriteRenderer();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            //src.enabled = true;
            sr.enabled = true;
        }
    }

    public void DisableSpriteRenderer()
    {
        //src.enabled = false;
        sr.enabled = false;
    }
}
