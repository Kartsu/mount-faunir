﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.MLAgents;
using Unity.MLAgents.Sensors;
using UnityEngine.Events;

public class AgentMovement : Agent
{
    private float directionX = 0f;
    private float directionY = 0f;
    private Rigidbody2D rb;
    private bool wasJumpPressed = false;
    private Visuals visuals;
    private bool hasDoubleJumped = false;
    private bool wasGrounded = false;
    public bool forceRightKeyPress = false;
    public bool forceJumpPress = false;
    public bool isDead = false;
    public bool isReverse = false;
    public Transform respawnPoint;

    [SerializeField]
    UnityEvent m_Respawn = default;
    public event UnityAction Respawned
    {
        add { m_Respawn.AddListener(value); }
        remove { m_Respawn.RemoveListener(value); }
    }
    void OnRespawn()
    {
        m_Respawn.Invoke();
    }

    public AudioSource audioSource;
    [HideInInspector]
    public bool isGrounded;

    [Header("Speeds")]
    public float walkSpeed = 0f;
    public float jumpForce = 0f;
    public float doubleJumpCooldown = 0.3f;
    public bool canMove;
    public bool canDoubleJump;

    [Header("Collision checking")]

    public Vector2 bottomOffset;


    public LayerMask groundLayer;

    public float collisionRadius = 0.25f;

    [Header("Polish")]

    public ParticleSystem jumpParticle;
    public ParticleSystem landParticle;
    public ParticleSystem doubleJumpParticle;
    public ParticleSystem deathParticle;
    public GameObject imageDie;
    public GameObject imageRespawn;
    public float respawnTime = 3.0f;

    [Header("Sounds")]

    public AudioClip doubleJumpSound;
    public AudioClip landSound;
    public AudioClip jumpSound;
    public AudioClip deathSound;



    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        visuals = GetComponentInChildren<Visuals>();
        audioSource = GetComponent<AudioSource>();
        m_Respawn.AddListener(Respawn);
        Respawn();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public override void Heuristic(float[] actionsOut)
    {
        actionsOut[0] = 0f;
        actionsOut[1] = 0f;
        if (canMove)
        {
            if (Input.GetAxisRaw("Horizontal") == -1)
            {
                actionsOut[0] = 1f;
            }
            else if (Input.GetAxisRaw("Horizontal") == 1)
            {
                actionsOut[0] = 2f;
            }
            //actionsOut[1] = Input.GetButtonDown("Jump") ? 1.0f : 0.0f;
            if (Input.GetButton("Jump"))
            {
                if (wasJumpPressed == false)
                {
                    wasJumpPressed = true;
                    actionsOut[1] = 1f;
                }
            }
            else
            {
                if (wasJumpPressed)
                {
                    wasJumpPressed = false;
                }
            }
        }

        else
        {
            if (forceRightKeyPress)
            {
                actionsOut[0] = 2f;
            }

            if (forceJumpPress)
            {
                actionsOut[1] = 0f;
            }
        }
    }

    public override void CollectObservations(VectorSensor sensor)
    {
        sensor.AddObservation(gameObject.transform.position.x);
        sensor.AddObservation(gameObject.transform.position.y);
    }

    public override void OnActionReceived(float[] act)
    {
        if (!isDead)
        {
            directionX = 0;
            directionY = 0;
            // Get the action index for movement
            int movement = Mathf.FloorToInt(act[0]);
            if (forceRightKeyPress)
            {
                movement = 2;
                if (Mathf.FloorToInt(act[0]) == 1)
                {
                    movement = 0;
                }
            }
            // Get the action index for jumping
            int jump = Mathf.FloorToInt(act[1]);
            if (forceJumpPress)
            {
                jump = 1;
                if (Input.GetButton("Jump"))
                {
                    jump = 0;
                }
            }
            // Look up the index in the movement action list:
            if (movement == 1)
            {
                if (!isReverse)
                {
                    directionX = -1;
                }
                else
                {
                    directionX = 1;
                }
            }
            if (movement == 2)
            {
                if (!isReverse)
                {
                    directionX = 1;
                }
                else
                {
                    directionX = -1;
                }
            }

            //Update isGrounded
            UpdateIsGrounded();

            // Look up the index in the jump action list:
            if (jump == 1)
            {
                if (isGrounded && canDoubleJump)
                {
                    audioSource.PlayOneShot(jumpSound);
                    directionY = 1;
                    StartCoroutine(DoubleJumpWait());
                }
                else if (!hasDoubleJumped && canDoubleJump)
                {
                    audioSource.PlayOneShot(doubleJumpSound);
                    directionY = 1;
                    hasDoubleJumped = true;
                    doubleJumpParticle.Play();
                }
            }

            // Apply the action results to move the Agent
            Vector2 dir = new Vector2(directionX, directionY);
            if (directionY == 1)
            {
                Jump(Vector2.up);
            }
            Walk(dir);
            visuals.SetHorizontalMovement(directionX, directionY, rb.velocity.y);
            if (directionX != 0)
            {
                visuals.Flip((int)directionX);
            }
        }
    }

    private void Jump(Vector2 dir)
    {
        visuals.anim.SetTrigger("jump");
        rb.velocity = new Vector2(rb.velocity.x, 0);
        rb.velocity += dir * jumpForce;
        jumpParticle.Play();
    }

    private void Walk(Vector2 dir)
    {
        rb.velocity = new Vector2(dir.x * walkSpeed, rb.velocity.y);
    }

    public void UpdateIsGrounded()
    {
        //Check if grounded :D
        isGrounded = Physics2D.OverlapCircle((Vector2)transform.position + bottomOffset, collisionRadius, groundLayer);
        if (isGrounded)
        {
            hasDoubleJumped = false;
        }
        if (wasGrounded == false && isGrounded)
        {
            onLand();
        }
        wasGrounded = isGrounded;
    }

    private void onLand()
    {
        landParticle.Play();
        audioSource.PlayOneShot(landSound);
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere((Vector2)transform.position + bottomOffset, collisionRadius);
    }

    IEnumerator DoubleJumpWait()
    {
        canDoubleJump = false;

        yield return new WaitForSeconds(0.3f);

        canDoubleJump = true;
    }

    public void EnableMovement()
    {
        canMove = true;
    }

    public void DisableMovement()
    {
        canMove = false;
    }

    public IEnumerator ForceRightKeyPress(float duration)
    {
        canMove = false;
        forceRightKeyPress = true;
        yield return new WaitForSeconds(duration);
        forceRightKeyPress = false;
        StartCoroutine(WaitForDuration(2));
    }

    public IEnumerator WaitForDuration(float duration)
    {
        yield return new WaitForSeconds(duration);
        canMove = true;
    }

    public void Die()
    {
        deathParticle.Play();
        audioSource.PlayOneShot(deathSound);
        rb.isKinematic = true;
        rb.constraints = RigidbodyConstraints2D.FreezeAll;
        StartCoroutine(Dead());
    }
    IEnumerator Dead()
    {
        canMove = false;
        isDead = true;
        imageRespawn.SetActive(false);
        imageDie.SetActive(true);
        visuals.GetComponent<SpriteRenderer>().enabled = false;
        yield return new WaitForSeconds(respawnTime);
        OnRespawn();
    }

    public void Respawn()
    {
        rb.isKinematic = false;
        rb.constraints = RigidbodyConstraints2D.FreezeRotation;
        imageDie.SetActive(false);
        imageRespawn.SetActive(true);
        transform.position = respawnPoint.transform.position;
        isDead = false;
        canMove = true;
        visuals.GetComponent<SpriteRenderer>().enabled = true;
    }
}