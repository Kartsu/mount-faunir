﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageManager : MonoBehaviour
{
    public GameObject virtualCam;
    public Transform respawnPoint;


    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            virtualCam.SetActive(true);
            collision.GetComponent<AgentMovement>().respawnPoint = respawnPoint;
        }
    }

    public void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            virtualCam.SetActive(false);
        }
    }
}