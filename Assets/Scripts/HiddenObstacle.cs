﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HiddenObstacle : MonoBehaviour
{

    private SpriteRenderer sr;
    public GameObject video;
    public GameObject placeholderObstacle;
    public GameObject dialogue;
    public bool isObstacle = false;


    private void Awake()
    {
        sr = GetComponent<SpriteRenderer>();
        sr.enabled = false;
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            if (!collision.GetComponent<AgentMovement>().isDead)
            {
                if (video != null)
                {
                    if (!video.activeSelf)
                    {
                        video.SetActive(true);
                        collision.GetComponent<AgentMovement>().Respawned += ReplaceWithPlaceholder;
                    }
                }
                if (isObstacle)
                {
                    sr.enabled = true;
                    collision.GetComponent<AgentMovement>().Die();
                }
            }
        }
    }

    private void ReplaceWithPlaceholder()
    {
        placeholderObstacle.SetActive(true);
        dialogue.SetActive(true);
        if (isObstacle)
        {
            gameObject.SetActive(false);
        }
        if(!isObstacle)
        {
            video.SetActive(false);
        }
    }
}
