﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public class ReverseTrigger : MonoBehaviour
{
    public PostProcessing postProcessing;
    private AgentMovement player;
    public AudioClip reverseSound;


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            player = collision.GetComponent<AgentMovement>();
            if (!player.isReverse)
            {
                player.isReverse = true;
                postProcessing.SetReversePostProcessing();
                player.audioSource.PlayOneShot(reverseSound);
                player.Respawned += Unreverse;
            }
        }
    }

    private void Unreverse()
    {
        if(player.isReverse)
        {
            player.audioSource.PlayOneShot(reverseSound);
            player.isReverse = false;
            postProcessing.SetNormalPostProcessing();
        }
        player.Respawned -= Unreverse;
    }
}
